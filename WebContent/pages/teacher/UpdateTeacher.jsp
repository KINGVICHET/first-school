<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.ProvinceBean" %>
<%@ page import="bean.CommuneBean" %>
<%@ page import="bean.CountryBean" %>
<%@ page import="bean.DistrictBean" %>
<%@ page import="db.services.CommuneService" %>
<%@ page import="bean.VillageBean" %>
<%@ page import="bean.TeacherBean" %>
<%@ page import="db.services.DistrictService" %>
<%@ page import="db.services.ProvinceService" %>
<%@ page import="db.services.VillageService" %>
<%@ page import="db.services.TeacherService" %>

    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>LMS</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <!-- Custom Theme Style -->
    <link href="../../css/custom.min.css" rel="stylesheet">
    
     <!-- jQuery -->
    <script src="../../js/jquery.min.js"></script>   
  	<script>
	$(document).ready(function(){
		$("#province").on('change', function(){
			var params = {proid : $(this).val()};
				$.post("${pageContext.request.contextPath}/ProvinceServlet" , $.param(params) , function(responseJson){
					var $select = $("#district");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.district_id ).text(key.district).appendTo($select);
					});
				});
		});
	});
	
	

	$(document).ready(function(){
		$("#district").on('change', function(){
			var params = {distid : $(this).val()};
				$.post("${pageContext.request.contextPath}/SelectCommuneServlet" , $.param(params) , function(responseJson){
					var $select = $("#commune");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.commune_id ).text(key.commune).appendTo($select);
					});
				});
		});
	});
		
	

	$(document).ready(function(){
		$("#commune").on('change', function(){
			
			var params = {comid : $(this).val()};				
				$.post("${pageContext.request.contextPath}/SelectVillageServlet" , $.param(params) , function(responseJson){
					var $select = $("#village");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.village_id ).text(key.village).appendTo($select);
					});
				});
		});
	});

	</script>
	
    		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

           
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>
					<%
                  		String teacher_id = request.getParameter("id");
             			TeacherBean sb = TeacherService.getATeacher(teacher_id);
             			String photo = "../.." + sb.getUrl() + "/" + sb.getT_photo();
             			
                  	 %>
					 
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                
                      <div class="x_title">
                          <h2 id="title_teacher">Update Teacher Information</h2>
                             <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                   </li>
                                     <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                         <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                   </li>
                                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                             </li>
                           </ul>
                         <div class="clearfix"></div>
                       </div>
                  
               <div class="x_content">
                 <br>
                  
                   <form class="form-horizontal  form-label-left" action="${pageContext.request.contextPath}/EditExistTeacher" Method="POST"  enctype="multipart/form-data">
                   
	                  <div class="col-md-6 col-sm-12" id="bordy_top" >
	                    
	                        <div class="form-group row" >
	                           <label class="control-label col-md-2 col-sm-2 col-xs-2">Teacher ID :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">                            
	                                  <input type="text" value="<%=sb.getT_id() %>" class="form-control" disabled id="teacher_id" >
	                               <input type="hidden" id="teacher_id"  name="teacher_id" value="<%=sb.getT_id() %>" class="form-control" autocomplete="off">
	                           </div>
	                      </div>
	                        
	                      <div class="form-group row" id="bordy_first_name">
	                          <label class="control-label col-md-2 col-sm-2 col-xs-2">First Name :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                        		 <small class="text-danger" id="fname_error"></small>
	                           	  <input type="text" class="form-control" value="<%=sb.getT_fname() %>"  id = "first_name"  name = "first_name" autocomplete="off">
	                           </div>
	                      </div>
	                      
	                     <div class="form-group row" id="bordy_last_name" >
	                         <label class="control-label col-md-2 col-sm-2 col-xs-2">Last Name :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">
	                              <input type="text" class="form-control" id= "last_name" value="<%=sb.getT_lname() %>" name="last_name" autocomplete="off">
	                        </div>
	                     </div>
	                     
					    <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Gender :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="add_error"></small>
	                                 <select class="form-control"​​​ id="gender" name="gender">
		                                <%
											ArrayList<TeacherBean> list = TeacherService.getATeacherGender();
											for( TeacherBean scBean : list)
												if(scBean.getT_gender().equals(sb.getT_gender())){
													out.print("<option value='" + scBean.getT_gender() + "' selected >'" + sb.getT_gender() + "</option>");
												}else{
													out.print("<option value='" + scBean.getT_gender() + "'>"+ scBean.getT_gender() + "</option>");
												}
										%>
                            	     </select>
                            	   <p>   </p>
	                           </div>
	                     </div>
					
	                      
	                     <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-2 col-sm-2 col-xs-2">Date of birth :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="dob_error"></small>
	                           <input type="text" class="form-control" name="dob" id="dob" autocomplete="off" value="<%= sb.getT_dob() %>">
	                        </div>
	                     </div>
	                     
	                     
                          <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Country :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                             <small class="text-danger" id="add_error"></small>
	                              <select class="form-control"​​​ id="control_id" name="control_id" >
	                             	 <option class="hidden"  selected disabled>Choose Country</option>
	                         				 <% 
                                	             	  
                                	                    
                                	             		
		                      		             %>
                            	  </select>
                            	 <p>   </p>
	                           </div>
	                       </div>
	                       
	                       <div class="form-group row"  id="bordy_dob">
	                          <label class="control-label col-md-2 col-sm-2 col-xs-2">Province :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                                 <small class="text-danger" id="add_error"></small>
	                                    <select class="form-control"​​​ id="province" name="province">
	                             	        <option class="hidden"  selected disabled   >Choose Province</option>
                                	             <% 
                                	             	  
                                	                    
                                	             		ArrayList<ProvinceBean> pbean = ProvinceService.p_listAllProvince();
                                	             		
		                      		                   for(ProvinceBean pb :  pbean)
					                      		         {
			                      		            	   if(pb.getPro_id().equals(sb.getaddress_teacher().getProvince_id())){
					                      			      out.print("<option value='"+ pb.getPro_id()+"' selected>" + pb.getProvince()+ "</option>");
					                      		        } else{
					                      		        	 out.print("<option value='"+ pb.getPro_id()+"'>" + pb.getProvince()+ "</option>");
					                      		           }
			                      		            	}
		                      		             %>
                            	          </select>
                            	      <p>   </p>
	                             </div>
	                         </div>
                               
                               
                         <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-2 col-sm-2 col-xs-2">District :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="add_error"></small>
	                                   <select class="form-control"​​​ id="district"  name="district" >
	                             	       <option class="hidden"  selected disabled>Choosse District</option>
				                             	<%
				                             		String province_id = sb.getaddress_teacher().getProvince_id();
				                           			String district_id = sb.getaddress_teacher().getDistrict_id();
				                             	   ArrayList<DistrictBean> dbean = DistrictService.p_listAllDistrictByProvinceID(province_id);
	                      		                    for(DistrictBean ad : dbean ) {
		                      		            	   if( ad.getDistrict_id().equals(district_id)){
				                      			      		out.print("<option value='"+ ad.getDistrict_id() +"' selected >" + ad.getDistrict()+ "</option>");
				                      		        	} else{
				                      		        	 		out.print("<option value='"+ ad.getDistrict_id() +"'>" + ad.getDistrict()+ "</option>");
				                      		           		}
		                      		            	}
					                             
					                      		%>
                            	         </select>
                            	     <p>   </p>
	                             </div>
	                      </div>
	                      
	                       
                       <div class="form-group row"  id="bordy_dob">
	                       <label class="control-label col-md-2 col-sm-2 col-xs-2">Commune:</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="add_error"></small>
	                                  <select class="form-control"​​​ id="commune" name="commune" >
	                             	    <option class="hidden"  selected disabled>Choose Commune</option>
	                             	         <%
				                             	   ArrayList<CommuneBean> cbean = CommuneService.p_listAllCommuneByDistrictID(sb.getaddress_teacher().getDistrict_id());
	                      		                    for( CommuneBean cd : cbean  )
				                      		         {
		                      		            	   if( cd.getCommune_id().equals(sb.getaddress_teacher().getCommune_id())){
				                      			      out.print("<option value='"+ cd.getCommune_id() +"' selected >" + cd.getCommune()+ "</option>");
				                      		        } else{
				                      		        	 out.print("<option value='"+ cd.getCommune_id()+"'>" + cd.getCommune()+ "</option>");
				                      		           }
		                      		            	}
					                             
					                      		%>
                            	     </select>
                            	  <p>   </p>
	                          </div>
	                    </div>
	                     
	                       
                        <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Village :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="add_error"></small>
	                                 <select class="form-control"​​​ id="village" name="village_id" >
	                             	    <option class="hidden"  selected disabled>Choose Village</option>
			                             	   <%
				                             	   ArrayList<VillageBean> vbean = VillageService.p_listAllVillageByCommuneID(sb.getaddress_teacher().getCommune_id());
	                      		                    for( VillageBean vd : vbean  )
				                      		         {
		                      		            	   if( vd.getVillage_id().equals(sb.getaddress_teacher().getVillage_id())){
				                      			      out.print("<option value='"+ vd.getVillage_id() +"' selected >" + vd.getVillage()+ "</option>");
				                      		        } else{
				                      		        	 out.print("<option value='"+ vd.getVillage_id() +"'>" + vd.getVillage()+ "</option>");
				                      		           }
		                      		            	}
					                             
					                      		%>
			                             	    				                      		  
                            	        </select>
                            	      <p>   </p>
	                             </div>
	                        </div>
	                  </div>
	                  
						
	                        
	                    
	                      <div class="col-md-6 col-sm-12" id ="bordy_phone_nuber">
		                      <div class="form-group row">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Phone Number :</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                          <small class="text-danger" id="phone_number_error"></small>
		                          <input type="text" class="form-control" id="phone_number" name="phone_number" value="<%=sb.getT_phone() %>" >
		                        </div>
		                      </div>
		                      
		                     <div class="form-group row"  id="bordy_dob">
	                            <label class="control-label col-md-2 col-sm-2 col-xs-2">Nationality :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="add_error"></small>
	                                 <select class="form-control"​​​ id="nationality" name="nationality" >
	                             	 <option class="hidden"  selected disabled>Choose Nationality</option>
	                             	
	                            		<%
	                            		ArrayList<TeacherBean> national = TeacherService.P_listInformation_Teacher();
										  for( TeacherBean scBean : national)
											if(scBean.getNationality().equals(sb.getNationality())){
												out.print("<option value='" + scBean.getNationality() + "' selected >'" + scBean.getNationality_id() + "</option>");
											}else{
												out.print("<option value='" + scBean.getNationality() + "'>"+ scBean.getNationality() + "</option>");
											}    
		                      		     %>
                            	    </select>
                            	<p>   </p>
	                         </div>
	                      </div>
		                      
		                      <div class="form-group row" id="bordy_education_id">
		                          <label class="control-label col-md-2 col-sm-2 col-xs-2"> Education Id :</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="education_id_error"></small>
		                           <input type="text" class="form-control" id="education_id" name="education_id" value="<%= sb.getEducation_id() %>">
		                         </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_passport_no">
		                          <label class="control-label col-md-2 col-sm-2 col-xs-2"> Nationality_Id:</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="passport_no_error"></small>
		                            <input type="text" class="form-control"  id="nationality_id" name="nationality_id" autocomplete="off" value="<%= sb.getNationality_id() %>">
		                          </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_stutus">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-2"> Passport No:</label>
		                           <div class="col-md-8 col-sm-9 col-xs-9">
		                                <small class="text-danger" id="status_error"></small>
		                             <input type="text" class="form-control"  id="passport_no" name="passport_no" autocomplete="off"  value="<%= sb.getPassport_no() %>">
		                          </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_stutus">
		                        <label class="control-label col-md-2 col-sm-2 col-xs-2"> Status:</label>
		                           <div class="col-md-8 col-sm-9 col-xs-9">
		                              <small class="text-danger" id="status_error"></small>
		                           <input type="text" class="form-control"  id="status" name="status"  autocomplete="off" value="<%= sb.getStustus() %>">
		                         </div>
		                      </div>
		                   
		                      <div class="form-group row">
		                          <label class="control-label col-md-2 col-sm-2 col-xs-2">Photo</label>
		                              <div class="col-md-8 col-sm-9 col-xs-9">
		                                 <div class="form-group">
                                          	<img alt="register_photo"  src="<%=photo %>" name="photo" id="show_photo" width="130px" height="147px" style="border:1px solid gray; border-radius:2px !important;">
                                          	<input type="hidden" name="old_photo" value="<%=sb.getT_photo() %>" />
                                              </div>
                                	          <div class="custom-file mb-3">
								    	    <input type="file" class="custom-file-input"  id="customFile" name="file" onchange="readDisplayUrl(this);" value="<%=sb.getUrl() %>>">
								   	 	 <label class="custom-file-label" for="customFile" >Choose your photo *</label>
		                       		   </div>
		                      	    </div>
		                       </div>
		                      
		                      <div class="form-group row" >
		                          <div class="col-md-12 col-sm-12 col-xs-12 offset-md-3">
		                            <button submit="submit"  class="btn btn-primary btn_cancel" onClick="Cancel()">Cancel</button>
		                            <button type="submit" class="btn btn-success" onClick="save()">Save</button>
		                        </div>
		                      </div>
		                      	                      	
	                      </div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
    </div>
	<script>
	     function readDisplayUrl(input){
	     	if(input.files && input.files[0]){
	     		var reader = new FileReader();
	     		reader.onload = function(e){
	     			$('#show_photo').attr('src', e.target.result)
	     		};
	     		reader.readAsDataURL( input.files[0] );
	     	}
	     }
	</script>
	<script src="../../js/custom_js/Input_Show_Image.js"></script>
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>

</body>
</html>