<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.services.TeacherService" %>
<%@ page import="bean.TeacherBean" %>
<%@ page import="db.services.TeacherService" %>
<%@ page import="db.services.DistrictService" %>
<%@ page import="db.services.ProvinceService" %>
<%@ page import="db.services.CommuneService" %>
<%@ page import="db.services.VillageService" %>

<%@ page import="bean.ProvinceBean" %> 
<%@ page import="bean.DistrictBean" %>
<%@ page import="bean.CommuneBean" %> 
<%@ page import="bean.VillageBean" %>   
<%@ page import="bean.CountryBean" %>  
    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>LMS</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet"><!-- Font Awesome -->
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <link href="../../css/custom.min.css" rel="stylesheet"> <!-- Custom Theme Style -->
    <link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="../../css/dataTable/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/scroller.bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script> 
    <script src="../../js/custom_js/lock_screen.js"></script>    <!-- LOCKSCREEN -->
  </head>
  
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>
     <div class="right_col" role="main">
        	<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List Teacher Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                    		<div class="container">
					            <div class="modal fade" id="myModal">
					                 <div class="modal-dialog">
					                      <div class="modal-content">
					                          <div class="modal-header">
					                              <h4 class="modal-title"><i class="fa fa-warning text-danger"></i> Message </h4>
					                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
					                                     </div>
					                                     <div class="modal-body">
					                                     <h5><i class="fa fa-info-circle text-danger"></i> Do you want to delete this Teacher?</h5>
					                                      <h3 id="showTeachertName"></h3>
					                                      </div>
					                                 <div class="modal-footer">
					        	                  <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
					                     	  <button type="submit" name="yes" id="btnyes" class="btn btn-danger" data-dismiss="modal" onclick="ConfirmDelete();">Yes</button>
					                    </div>
					                 </div>
					             </div>
					        </div>
					    </div>
                    
                    
                  </div>
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                   			<div class="card-box table-responsive">
								<a style="margin-left:15px;" href="../../view/Teacher/AddTeacher" class="btn btn-info btn-sm"><i class="fa fa-plus"> New Teacher</i></a>
								<a style="margin-left:15px;" href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"> View Teacher Detail</i></a>
								<a style="margin-left:15px;" href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"> New Teacher</i></a>
				                <div id="datatable-responsive_wrapper" class="dataTables_wrapper container dt-bootstrap no-footer">
					              
				                   <div class="row">
				                    	<div class="col-sm-12">
				                    		<table id="datatable-responsive" class="table table table-hover table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-responsive_info" style="width: 100%;">
							                      <thead>
							                      
							                        <tr role="row" class="table-success text-center">
							                        	<th class="sorting" style="width: 68px;">Teacher ID</th>
							                        	<th class="sorting" style="width: 68px;">Full Name</th>
							                        	<th class="sorting" style="width: 153px;">Gender</th>
							                        	<th class="sorting" style="width: 153px;">Nationality</th>
							                        	<th class="sorting" style="width: 153px;">Nationality_Id</th>
							                        	<th class="sorting" style="width: 26px;">Date of birth</th>
							                        	<th class="sorting" style="width: 153px;">Current Address</th>
							                        	<th class="sorting" style="width: 33px;">Phone Number</th>	
							                        	<th class="sorting" style="width: 33px;">Passport No:</th>
							                        	<th class="sorting" style="width: 33px;">Status :</th>							                        						                        	               	
							                        	<th class="sorting" style="width: 144px;">Education Id</th>
							                        	<th class="sorting" style="width: 33px;"> Image:</th>
							                        	<th class="sorting" style="width: 33px;"> OPtion</th>
							                        	
							                        	
							                        </tr>
							                      </thead>
							                      <tbody>
							                      	<%
							                      		ArrayList<TeacherBean> list = TeacherService.P_listInformation_Teacher();
						                      		for(TeacherBean sb : list){
						                      			out.print("<tr role='row' class='odd'>");
						                      				out.print("<td class='' tabindex='0'>" + sb.getT_id() + "</td>");
						                      				String Full_name = sb.getT_fname()+ sb.getT_lname();
						                      				out.print("<td '>"+ Full_name  + "</td>");
						                    				out.print("<td>" + sb.getT_gender() + "</td>");
						                      				out.print("<td class=''>" + sb.getNationality() + "</td>");
						                      				out.print("<td>" + sb.getNationality_id() + "</td>");
						                      				out.print("<td class=''>" + sb.getT_dob() + "</td>");
						                      				String current_address = sb.getaddress_teacher().getVillage()+" " + sb.getaddress_teacher().getCommune()+" "+sb.getaddress_teacher().getDistrict()+" "+sb.getaddress_teacher().getProvince();
						                      				out.print("<td class='sorting_1'>"+ current_address +"</td>");
						                      				out.print("<td class=''>" + sb.getT_phone() + "</td>");
						                      				out.print("<td>" + sb.getPassport_no() + "</td>");
						                      				out.print("<td class=''>" + sb.getStustus() + "</td>");
						                      				out.print("<td class=''>" + sb.getEducation_id() + "</td>");
						                      				String photo = "..\\.." + sb.getUrl() + sb.getT_photo();
				                                            out.print("<td class=''><img src='" + photo + "' widht='50px' height='50px'></td>");
						                      				out.print("<td class='text-center'>");
					                      						out.print("<a href='../../view/update/UpdateATeacher?id=" + sb.getT_id() +  "' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a>");
					                      						out.print("<button type='button' value='" +"' class='btn btn-danger btn-sm btnDelete' data-toggle='modal' data-target='#myModal'><i class='fa fa-trash'></i></button>");
						                      				out.print("</td>");
						                      			out.print("</tr>");
						                      		}
						                      	%>
							                	</tbody>
				                    		</table>
				                  		</div>
				                 	</div>
				               </div>
                  			</div>
                		</div>
              		</div>
            	</div>
         	</div>
       	</div>  
     </div>

		
		<jsp:include page="/view/detail/FooterPage"></jsp:include>
      </div>
    </div>

    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/dataTable/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTable/dataTables.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.responsive.min.js"></script>
    <script src="../../js/dataTable/responsive.bootstrap.js"></script>
    <script src="../../js/dataTable/buttons.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.scroller.min.js"></script>
    <script src="../../js/custom.min.js"></script>
	<script>
    	$("#datatable-responsive").on("click", ".btnDelete", function(e){
    		var currentStudentName = $(this).val();
    		$("#showStudentName").text(currentStudentName);
    	});
    </script>	
</body>
</html>