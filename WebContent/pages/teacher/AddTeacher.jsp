<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.ProvinceBean" %>
<%@ page import="bean.CommuneBean" %>
<%@ page import="bean.CountryBean" %>
<%@ page import="bean.DistrictBean" %>
<%@ page import="bean.VillageBean" %>
<%@ page import="bean.TeacherBean" %>
<%@ page import="db.services.ProvinceService" %>
<%@ page import="db.services.TeacherService" %>
    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>LMS</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <!-- Custom Theme Style -->
    <link href="../../css/custom.min.css" rel="stylesheet">
    
     <!-- jQuery -->
    <script src="../../js/jquery.min.js"></script>   
  	<script>
	$(document).ready(function(){
		$("#province").on('change', function(){
			var params = {proid : $(this).val()};
				$.post("${pageContext.request.contextPath}/ProvinceServlet" , $.param(params) , function(responseJson){
					var $select = $("#district");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.district_id ).text(key.district).appendTo($select);
					});
				});
		});
	});
	
	

	$(document).ready(function(){
		$("#district").on('change', function(){
			var params = {distid : $(this).val()};
				$.post("${pageContext.request.contextPath}/SelectCommuneServlet" , $.param(params) , function(responseJson){
					var $select = $("#commune");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.commune_id ).text(key.commune).appendTo($select);
					});
				});
		});
	});
		
	

	$(document).ready(function(){
		$("#commune").on('change', function(){
			
			var params = {comid : $(this).val()};				
				$.post("${pageContext.request.contextPath}/SelectVillageServlet" , $.param(params) , function(responseJson){
					var $select = $("#village");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key){
						console.log( key );
						$("<option>").val( key.village_id ).text(key.village).appendTo($select);
					});
				});
		});
	});

	</script>
 		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="title_student">Add Student Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                    
                     <%
						if( session.getAttribute("code") != null ) 
						{
							String msg = session.getAttribute("code").toString();
							if( msg.equalsIgnoreCase("error") )
							 {
								out.print("<div class='alert alert-danger' id='danger-alert'>");
								out.print("<h3>ERROR</h3>");
								out.print("Insert data failed.");
								out.print("</div>");
							%>	
								<script type="text/javascript">
									
									    $("#danger-alert").fadeTo( 2000 , 500 ).slideUp(500, function(){
									    	$("#danger-alert").slideUp(500);
									    });
									
								</script>
							<% 	
							
							
							
						 }
				
							else{
								out.print("<div class='alert alert-success' id='success-alert'>");
								out.print("<h4>Success</h4>");
								out.print("Insert data was successful.");
								out.print("</div>");
							%>	
								<script type="text/javascript">
								
									 $("#success-alert").fadeTo( 2000 , 500 ).slideUp(500, function(){
									 $("#success-alert").slideUp(500);
									 });
								
								
									
								</script>
							<% 	
							}
                     }
						
						
					%>
                    
                  </div>
                  <div class="x_content">
                  <br>
                   <form class="form-horizontal  form-label-left" action="${pageContext.request.contextPath}/AddANewTeacherServlet" Method="POST"  enctype="multipart/form-data" >
                    
	                    <div class="col-md-6 col-sm-12" id="bordy_top" >
	                      <div class="form-group row" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Teacher ID :</label>
	                        <div class="col-md-8 col-sm-9 col-xs-9">
	                             <%
	                        		String teacher_id = TeacherService.getteacherId();
	                        	 %>
	                          <input type="text" name="teacher_id" id="teacher_id" value="<%=teacher_id  %>" class="form-control" disabled>
	                          <input type="hidden" name="teacher_id" id="teacher_id" value="<%= teacher_id %>" class="form-control">
	                        </div>
	                      </div>
	                      <div class="form-group row" id="bordy_first_name">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">First Name :</label>
	                        <div class="col-md-8 col-sm-9 col-xs-9">
	                        		  <small class="text-danger" id="fname_error"></small>
	                           	      <input type="text" class="form-control" id="first_name" name = "first_name" placeholder= "Teacher_First_Name" autocomplete="off" >
	                        </div>
	                      </div>
	                      
	                     <div class="form-group row" id="bordy_last_name" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Last Name :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                            
	                            <input type="text" class="form-control" id= "last_name" name="last_name" placeholder ="Teacher_Last_Name" autocomplete="off" >
	                        </div>
	                     </div>
	                      
	                      <div class="form-group row" id="bordy_gender">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Gender :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                             <small class="text-danger" id="gender_error"></small>
	                              <select class="form-control" name="gender" id="gender">
                                	 <option class="hidden" selected disable>Choose Gender</option>
			                      		<option class="hidden" value="Male"> Male</option>
			                      		<option class="hidden"  value="female"> Female</option>
			                         <option class="hidden"  value="Other"> Other</option>
                            	</select>
	                         </div>
	                      </div>
	                      
	                     <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Date of birth :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="dob_error"></small>
	                           <input type='date' class="form-control" id="dob" name="dob" placeholder ="YYYY-MMM-DD" autocomplete="off">
	                        </div>
	                     </div>
	                     
	                     
                          <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Country :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="con_id" name="con_id" >
	                             	<option class="hidden"  selected disabled>Choose Country</option>
	                             	<option class="hidden"  value="ប្រទេសកម្ពុជា">ប្រទេសកម្ពុជា</option>
	                             	<option class="hidden"  value="ប្រទេសថៃ">ប្រទេសថៃ</option>
	                             	<option class="hidden"  value="ប្រទេសឡាវ">ប្រទេសឡាវ</option>
	                             	<option class="hidden"  value="ប្រទេសឡាវ">ប្រទេសឡាវ</option>
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
	                       
	                       <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Province :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="province" name="province" >
	                             	<option class="hidden"  selected disabled>Choose Province</option>
                                	<%
                            		          ArrayList<ProvinceBean >ListPro= ProvinceService.p_listAllProvince();
			                      		        for(ProvinceBean pb : ListPro)
				                      		        {
				                      			      out.print("<option value='"+ pb.getPro_id()+"'>" + pb.getProvince() + "</option>");
				                      		        }
		                      		%>
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
                               
                               
                                <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">District :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="district" name="district" >
	                             	<option class="hidden"  selected disabled>Choose District</option>
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
	                     
	                       
                                <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Commune:</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="commune" name="commune" >
	                             	<option class="hidden"  selected disabled>Choose Commune</option>
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
	                     
	                       
                                <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Village :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="village" name="village_id" >
	                             	<option class="hidden"  selected disabled>Choose Village</option>
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
                               
                               <div class="form-group row" >
		                        <div class="col-md-8 col-sm-9 col-xs-9 offset-md-3">
		                          <button type="submit"  class="btn btn-primary btn_cancel" onClick="Cancel()">Cancel</button>
		                          <button type="submit" class="btn btn-success" onClick="save()">Save</button>
		                        </div>
		                      </div>
                               
	                        </div>
	                        
	                    
	                      <div class="col-md-6 col-sm-12" id ="bordy_phone_nuber">
		                      <div class="form-group row">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Phone Number :</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                          <small class="text-danger" id="phone_number_error"></small>
		                          <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder ="Teacher_Phone_Number" autocomplete="off">
		                        </div>
		                      </div>
		                      
		                     <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Nationality :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="add_error"></small>
	                           <select class="form-control"​​​ id="nationality" name="nationality" >
	                             	<option class="hidden"  selected disabled >Choose Nationality</option>
	                             	<option class="hidden"  value="ខ្មែរ">ខ្មែរ</option>
	                            
                            	</select>
                            	<p>   </p>
	                        </div>
	                     </div>
		                      
		                      <div class="form-group row" id="bordy_education_id">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3"> Education Id :</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                          <small class="text-danger" id="education_id_error"></small>
		                          <input type="text" class="form-control" id="education_id" name="education_id" placeholder ="Teacher_Education_Id" autocomplete="off">
		                        </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_passport_no">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3"> Nationality_Id:</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                         <small class="text-danger" id="passport_no_error"></small>
		                          <input type="text" class="form-control" id="nationality_id" name="nationality_id" placeholder ="Natoinality Id" autocomplete="off">
		                        </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_stutus">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3"> Passport No:</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                           <small class="text-danger" id="status_error"></small>
		                          <input type="text" class="form-control" id="passport_no" name="passport_no" placeholder ="Teacher_Passport_No" autocomplete="off">
		                        </div>
		                      </div>
		                      
		                      <div class="form-group row" id="bordy_stutus">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3"> Status:</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                           <small class="text-danger" id="status_error"></small>
		                          <input type="text" class="form-control" id="status" name="status" placeholder ="Teacher_Status" autocomplete="off">
		                        </div>
		                      </div>
		                   
		                      <div class="form-group row">
		                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Photo</label>
		                        <div class="col-md-8 col-sm-9 col-xs-9">
		                          <div class="form-group">
                                      	<img alt="register_photo" src="../../images/Avatar/register_avatar.jpg" name="photo" id="show_photo" width="130px" height="147px" style="border:1px solid gray; border-radius:2px !important;">
                                  </div>
                                	<div class="custom-file mb-3">
								    	<input type="file" class="custom-file-input" id="customFile" name="file" onchange="readDisplayUrl(this);">
								   	 	<label class="custom-file-label" for="customFile">Choose your photo *</label>
		                       		</div>
		                      	</div>
		                      </div>
		                                        
		                      	                      	
	                      </div>   
                    </form>
                  </div>
                </div>
              </div>
        </div>
		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
    </div>
	
	<script src="../../js/custom_js/Input_Show_Image.js"></script>
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>

</body>
</html>