<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.EducationBean" %>
<%@ page import="db.services.EducationService" %>


    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />


    <title>LMS</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <!-- Custom Theme Style -->
    <link href="../../css/custom.min.css" rel="stylesheet">
    <link href="../../css/customStyle/CssStyleValidetion.css" rel="stylesheet">
    <link href="../../css/customStyle/ValidatecssEducation.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script> 
  <script src="../../js/popper.min.js"></script>
  <script src="../../js/bootstrap.min.js"></script>  
  	
    		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>

        <!-- page content -->
         <div class="right_col" role="main">              
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                
                      <div class="x_title">
                          <h2 id="title_teacher">AddNew Teacher Education </h2>
                             <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                   </li>
                                     <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                         <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                   </li>
                                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                             </li>
                           </ul>
                         <div class="clearfix"></div>
                       </div>
                  <%
                  	String edu_id = EducationService.getNewEducationIdAuto();
                  %>
               <div class="x_content">
                 <br>
                  
                   <form class="form-horizontal  form-label-left" action="${pageContext.request.contextPath}/AddNewTeacherEducationServlet" id="formAdd"  Method="POST">
                   
	                  <div class="col-md-6 col-sm-12" id="bordy_top" >
	                    
	                        <div class="form-group row" >
	                           <label class="control-label col-md-4 col-sm-4 col-xs-4">Education ID :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">  
	                              	<input type="text" value="<%=edu_id%>" class="form-control" disabled >
	                              	<input type="hidden" name="education_id" value="<%=edu_id %>"/>
	                           </div>
	                      </div>
	                         <div class="input-icons">
	                      <div class="form-group row  " id="bordy_first_name">
	                          <label class="control-label col-md-4 col-sm-4 col-xs-4">School Name :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                        		<small id="error-school_name"  class="text-danger font-weight-bold error-notfull "> </small>
	                           	  <input type="text" class="form-control success-full " id="school_name" name = "school_name" autocomplete="off" placeholder="School Name" >
	                           		
	                           </div>
	                      </div>
	                      
	                     <div class="form-group row error" id="bordy_last_name" >
	                         <label class="control-label col-md-4 col-sm-4 col-xs-4">Teacher Skill :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">
	                                  <small class="text-danger font-weight-bold "  id="error_skill"></small>
	                                <input type="text" class="form-control success-full" id="teacher_skill" name="teacher_skill" AutoComplete="off" placeholder="Education Skill" >
	                        		
	                        </div>
	                     </div>
	                     
	                     <div class="form-group row error" id="bordy_education_id">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> Education Level :</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger font-weight-bold error-notfull" id="error_level"></small>
		                              
		                             <input type="text" class="form-control success-full" id="teacher_level" name="education_level" autocomplete="off"  placeholder="Education Level" >
		                         	
		                         </div>
		                      </div>
					  					
	                      
	                     <div class="form-group row error"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-4 col-xs-4">Start To Learn :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger font-weight-bold error-notfull "  id="error_start"></small>
	                            
	                           <input type="date" class="form-control success-full" name="start_to_learn" id="start" autocomplete="off" placeholder="DD/MM/YYYY" >
	                        	
	                        </div>
	                     </div>                
	                    
	                    		                      
		                      <div class="form-group row error" id="bordy_passport_no">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> End To Learn:</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger font-weight-bold error-notfull"  id="error_end"></small>
		                            <input type="date" class="form-control success-full"  id="end" name="end_to_learn" autocomplete="off" placeholder="DD/MM/YYYY">
		                          	
		                          </div>
		                      </div>
		                      
		                      <div class="form-group row error"  id="bordy_dob">
	                               <label class="control-label col-md-4 col-sm-4 col-xs-4">FacBook :</label>
	                                <div class="col-md-8 col-sm-9 col-xs-9">
	                                <small class="text-danger font-weight-bold error-notfull" id="error_facebook"> </small>
	                                 <input type="text" class="form-control success-full" name="facebook" id="facebook" autocomplete="off" placeholder="FaceBook">
	                             </div>
	                           </div>                
	                      
		                      <div class="form-group row error" id="bordy_passport_no">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> Email :</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger font-weight-bold error-notfull" id="error_email"></small>
		                            <input type="text" class="form-control success-full"  id="email" name="email" autocomplete="off" placeholder="Email">
		                            
		                          </div>
		                      </div>
		                      </div>
		                      
		                      <div class="form-group row" >
		                          <div class="col-md-12 col-sm-12 col-xs-12 offset-md-3">
		                            <button type ="submit"  class="btn btn-danger btn_cancel" id="btn_cancel" name ="btn_cancel" OnClick="btnClear()">Clear</button>
		                            <button type ="button" class="btn btn-success "  onclick=" Mysave()" id="btnsave"  value="submit" >Save</button>
		                            <a type="button"  href="../../view/teacher_education/ListEducationTeacher" class="btn btn-primary " id="btn_back" name ="btn_back"  >Back</a>
		                        </div>
		                      </div>
		                      	                      	
	                      </div>
                    </form>
                  </div>
                </div>
              </div>
        </div>

		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
    </div>
    
	 <script src="../../js/custom_js/validation.js"></script>
	<script src="../../js/custom_js/Input_Show_Image.js"></script>
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>

</body>
</html>