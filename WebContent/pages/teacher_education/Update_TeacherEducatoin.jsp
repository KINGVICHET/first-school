<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.EducationBean" %>
<%@ page import="db.services.EducationService" %>


    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>LMS</title>

    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> 
    <!-- Custom Theme Style -->
    <link href="../../css/custom.min.css" rel="stylesheet">
    
     <!-- jQuery -->
    <script src="../../js/jquery.min.js"></script>   
    		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

           
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>

<div class="right_col" role="main">
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                
                      <div class="x_title">
                          <h2 id="title_teacher">Update Teacher Education </h2>
                             <ul class="nav navbar-right panel_toolbox">
                               <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                   </li>
                                     <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                         <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                   </li>
                                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                             </li>
                           </ul>
                         <div class="clearfix"></div>
                         
                          
                    		<div class="container">
					            <div class="modal fade" id="myModal">
					                 <div class="modal-dialog">
					                      <div class="modal-content">
					                          <div class="modal-header">
					                              <h4 class="modal-title"><i class="fa fa-warning text-danger"></i> Message </h4>
					                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
					                                     </div>
					                                     <div class="modal-body">
					                                     <h5><i class="fa fa-info-circle text-danger"></i> Do you want to delete this Teacher?</h5>
					                                      <h3 id="showTeachertName"></h3>
					                                      </div>
					                                 <div class="modal-footer">
					        	                  <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
					                     	  <button type="submit" name="yes" id="btnyes" class="btn btn-danger" data-dismiss="modal" onclick="ConfirmDelete();">Yes</button>
					                    </div>
					                 </div>
					             </div>
					        </div>
					    </div>
                         
                         
                         
                         
			                      <%
			                  		String education_id = request.getParameter("id");
			             			EducationBean eb =EducationService.getDataTeacherEducation(education_id);
			             			
			                  	 %>
                       </div>
                  
               <div class="x_content">
                 <br>
                                         
                                         
                   <form class="form-horizontal  form-label-left" action="${pageContext.request.contextPath}/UpdateEducationTeacher" Method="POST" >
                   
	                  <div class="col-md-6 col-sm-12" id="bordy_top" >
	                    
	                        <div class="form-group row" >
	                           <label class="control-label col-md-4 col-sm-4 col-xs-4">Education ID :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">                            
	                                  <input type="text"  class="form-control" name="education_id" value="<%=eb.getEducation_id() %>" disabled>
	                               <input type="hidden" id="education_id"  name="education_id"  value="<%=eb.getEducation_id() %>" class="form-control" autocomplete="off" >
	                           </div>
	                      </div>
	                        
	                      <div class="form-group row" id="bordy_first_name">
	                          <label class="control-label col-md-4 col-sm-4 col-xs-4">School Name :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                        		 <small class="text-danger" id="school_name_error"></small>
	                           	  <input type="text" class="form-control"   id = "school_name" value="<%=eb.getSchool_name() %>"  name = "school_name" autocomplete="off">
	                           </div>
	                      </div>
	                      
	                      
	                      
	                     
	                      
	                     <div class="form-group row" id="bordy_last_name" >
	                         <label class="control-label col-md-4 col-sm-4 col-xs-4">Teacher Skill :</label>
	                              <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="teacher_skill_error"></small>
	                              <input type="text" class="form-control" id= "teacher_skill"  value="<%=eb.getSkill() %>" name="teacher_skill" AutoComplete="off" value="">
	                        </div>
	                     </div>
	                     
	                     <div class="form-group row" id="bordy_education_id">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> Education Level :</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="education_level_error"></small>
		                           <input type="text" class="form-control" id="education_level" value="<%=eb.getLevel() %>" name="education_level" value="">
		                         </div>
		                      </div>
					  					
	                      
	                     <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-4 col-xs-4">Start To Learn :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="start_to_learn_error"></small>
	                           <input type="date" class="form-control" name="start_to_learn"  value="<%=eb.getStart() %>" id="start_to_learn" autocomplete="off" value="">
	                        </div>
	                     </div>                
	                    
	                    		                      
		                      <div class="form-group row" id="bordy_passport_no">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> End To Learn:</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="end_to_learn_error"></small>
		                            <input type="date" class="form-control"  id="end_to_learn" value="<%=eb.getEnd() %>" name="end_to_learn" autocomplete="off" value="">
		                          </div>
		                      </div>
		                      
		                      <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-4 col-xs-4">FacBook :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger" id="facebook_error"></small>
	                           <input type="text" class="form-control" name="facebook"  value="<%=eb.getFacbook() %>" id="facebook" autocomplete="off" placeholder="FaceBook" >
	                        </div>
	                     </div>                
	                    
	                    		                      
		                      <div class="form-group row" id="bordy_passport_no">
		                          <label class="control-label col-md-4 col-sm-4 col-xs-4"> Email :</label>
		                            <div class="col-md-8 col-sm-9 col-xs-9">
		                             <small class="text-danger" id="email_error"></small>
		                            <input type="text" class="form-control"  id="email"  value="<%=eb.getEmail() %>" name="email" autocomplete="off" placeholder="Email">
		                          </div>
		                      </div>
		                      
		                     
		                      
		                      <div class="form-group row" >
		                          <div class="col-md-12 col-sm-12 col-xs-12 offset-md-3">
		                            <button type="submit"  class="btn btn-danger btn_cancel" onClick="Clear()">Clear</button>
		                            <button type="submit" class="btn btn-success" OnClick="save()">Update</button>
		                            
		                            <a type="submit"  class="btn btn-primary btn_cancel"  href="../../view/teacher_education/ListEducationTeacher">Back</a>
		                        </div>
		                      </div>                    	
	                      </div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
    </div>
	
	<script src="/../js/custom_js/ClearTextBoxeducationTeacher.js"></script>
	<script src="../../js/custom_js/Input_Show_Image.js"></script>
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>

</body>
</html>