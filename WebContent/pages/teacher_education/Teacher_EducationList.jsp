<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.util.ArrayList" %>
<%@ page import="db.services.EducationService" %>
<%@ page import="bean.EducationBean" %> 

    
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>LMS</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap -->
    <link href="../../css/font-awesome.min.css" rel="stylesheet"><!-- Font Awesome -->
    <link href="../../webcss/all.min.css" rel="stylesheet"> <!--load all styles -->
    <link href="../../css/custom.min.css" rel="stylesheet"> <!-- Custom Theme Style -->
    <link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="../../css/dataTable/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../../css/dataTable/scroller.bootstrap.min.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script> 
    <script src="../../js/custom_js/lock_screen.js"></script>    <!-- LOCKSCREEN -->
    
    
  </head>
  
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>
     <div class="right_col" role="main">
        	<div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>List Teacher Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                   			<div class="card-box table-responsive">
								<a style="margin-left:15px;" href="../../view/teacher_education/AddEducationTeacher" class="btn btn-info btn-sm"><i class="fa fa-plus"> Add Teacher Education</i></a>
								<a style="margin-left:15px;" href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"> View Teacher Detail</i></a>
								<a style="margin-left:15px;" href="#" class="btn btn-info btn-sm"><i class="fa fa-plus"> New Teacher</i></a>
				                <div id="datatable-responsive_wrapper" class="dataTables_wrapper container dt-bootstrap no-footer">
					              
				                   <div class="row">
				                    	<div class="col-sm-12">
				                    		<table id="datatable-responsive" class="table table table-hover table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="datatable-responsive_info" style="width: 100%;">
							                      <thead>
							                      
							                        <tr role="row" class="table-success text-center">
							                        	<th class="sorting" style="width: 68px;">Education ID</th>
							                        	<th class="sorting" style="width: 68px;">School Name</th>
							                        	<th class="sorting" style="width: 153px;">Teacher Skill</th>
							                        	<th class="sorting" style="width: 153px;">Teacher Level</th>
							                        	<th class="sorting" style="width: 153px;">Start To Learn</th>
							                        	<th class="sorting" style="width: 26px;">End To Learn</th>
							                        	<th class="sorting" style="width: 153px;">FaceBook</th>
							                        	<th class="sorting" style="width: 33px;">Email </th>
							                        	<th class="sorting" style="width: 33px;">Option </th>		
							                        </tr>
							                      </thead>
							                      <tbody>
							                      	<%
							                      		ArrayList<EducationBean> list = EducationService.DisplayEducation();
						                      		for(EducationBean ed : list){
						                      			out.print("<tr role='row' class='odd'>");
						                      				out.print("<td class='' tabindex='0'>" + ed.getEducation_id() + "</td>");
						                      				out.print("<td '>"+ ed.getSchool_name()  + "</td>");
						                    				out.print("<td>" + ed.getSkill() + "</td>");
						                      				out.print("<td class=''>" + ed.getLevel() + "</td>");
						                      				out.print("<td>" + ed.getStart() + "</td>");
						                      				out.print("<td class=''>" + ed.getEnd() + "</td>");
						                      				out.print("<td class='sorting_1'>"+ ed.getFacbook() +"</td>");
						                      				out.print("<td class=''>" + ed.getEmail() + "</td>");
						                      				
						                      				out.print("<td class='text-center'>");
					                      						out.print("<a href='../../view/teacher_education/UpdateEducationTeacher?id=" + ed.getEducation_id()+  "' class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a>");
					                      						out.print("<a href='../../view/TeacherEducation/DeleteEducationByid?id=" + ed.getEducation_id()+  "' class='btn btn-danger btn-sm btnDelete'><i class='fa fa-trash'></a>");
					                      						
						                      				out.print("</td>");
						                      			out.print("</tr>");
						                      		}
						                      	%>
							                	</tbody>
				                    		</table>
				                  		</div>
				                 	</div>
				               </div>
                  			</div>
                		</div>
              		</div>
            	</div>
         	</div>
       	</div>  
     </div>

		<jsp:include page="/view/detail/FooterPage"></jsp:include>
      </div>
    </div>

    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/dataTable/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTable/dataTables.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.responsive.min.js"></script>
    <script src="../../js/dataTable/responsive.bootstrap.js"></script>
    <script src="../../js/dataTable/buttons.bootstrap.min.js"></script>
    <script src="../../js/dataTable/dataTables.scroller.min.js"></script>
    <script src="../../js/custom.min.js"></script>
	<script>
    	$("#datatable-responsive").on("click", ".btnDelete", function(e){
    		var currentStudentName = $(this).val();
    		$("#showStudentName").text(currentStudentName);
    	});
    </script>	
</body>
</html>