<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="bean.UserBean"%>
<%@ page import="db.services.UserService" %>
	<div class="container">
  <!-- The Modal -->
	  <div class="modal fade" id="myModal">
	    <div class="modal-dialog">
	      <div class="modal-content">
	      
	        <!-- Modal Header -->
	        <div class="modal-header">
	          <h4 class="modal-title">Verification!</h4>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <!-- Modal body -->
	        <div class="modal-body">
	          Do you want to delete this user?
	        </div>
	        <!-- Modal footer -->
	        <div class="modal-footer">
	        	<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
	          	<button type="submit" name="yes" id="btnyes" class="btn btn-danger" data-dismiss="modal" onclick="ConfirmDelete();">Yes</button>
	        </div>
	        
	      </div>
	    </div>
	  </div>
  
	</div>