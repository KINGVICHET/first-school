package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.DistrictBean;
import bean.ProvinceBean;
import db.mysql.MySQL;

public class DistrictService {
	
	public static String getNewId(){
		String code = null;
		
        try{
        	String sql = "SELECT MAX(district_id) AS districtID FROM t_district";
        	PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
        	ResultSet rs = ps.executeQuery();
        	if(rs.next()){
        		String dCode = rs.getString("districtID");
        		String l_code = l_cutePrefix(dCode, "dist");
        		int tmpCode = Integer.parseInt(l_code);
        		tmpCode ++;
        		
        		code = "dist";
        		code += String.format("%03d", tmpCode);
        	}
        	
        }catch(Exception e){
        	code = "dist";
        	code += String.format("%03d", 1);
        	System.out.println("DistrictService::getNewId() => " + e.toString());
        }finally{
        	MySQL.P_getClose();
        }
		
		return code;
	}
	
	//=========================================================================
		private static String l_cutePrefix(String str, String pref){
			if(str != null && pref != null && str.startsWith(pref)){
				return str.substring(pref.length());
			}
			return str;
		}
		
		public static ArrayList<DistrictBean> p_listAllDistrict(){
			ArrayList<DistrictBean> al = new ArrayList<DistrictBean>();
			try{
				String sql = "SELECT * FROM t_district";
				PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					DistrictBean db = new DistrictBean();
					db.setDistrict_id(rs.getString("district_id"));
					db.setDistrict(rs.getString("district"));
					db.setProvince_id(rs.getString("province_id"));
			
					al.add(db);
				}
			}catch(Exception e){
				System.out.println(" show error!" + e.toString());
			}finally{
				MySQL.P_getClose();
			}
			return al;
		}
	
	
	public static ArrayList<DistrictBean> p_listAllDistrictByProvinceID(String id){
		ArrayList<DistrictBean> al = new ArrayList<DistrictBean>();
		try{
			String sql = "SELECT * FROM t_district WHERE province_id = ?";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				DistrictBean db = new DistrictBean();
				db.setDistrict_id(rs.getString("district_id"));
				db.setDistrict(rs.getString("district"));
				db.setProvince_id(rs.getString("province_id"));
				
				al.add(db);
			}
		}catch(Exception e){
			System.out.println(" show error!" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return al;
	}
	
	public static String addNewDistrict(DistrictBean db){
		String message = "";
		try{
        	String sql = "INSERT INTO t_district VALUES(?,?,?)";
        	PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
        	ps.setString(1, db.getDistrict_id());
        	ps.setString(2, db.getDistrict());
        	
        	ProvinceBean pb = db.getPb();
        	ps.setString(3, pb.getPro_id());
			
        	int state = ps.executeUpdate();
        	if(state > 0 ){		
				message = "success";
			}
		}catch(Exception e){
			message = "error";
        	System.out.println("DistrictService::addNewDistrict() => " + e.toString());
        }finally{
        	MySQL.P_getClose();;
        }
		return message;
	}
}
