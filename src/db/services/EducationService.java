package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.EducationBean;
import db.mysql.MySQL;

public class EducationService {
	
	//============   Display All list teacher Education============
	public static ArrayList<EducationBean> DisplayEducation(){
		
		ArrayList<EducationBean >ed= new ArrayList<EducationBean>();
		try
			{
				String sql = "SELECT * FROM t_teacher_edu"; 
				PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				    while(rs.next())
				         {
				    	    EducationBean edb= new EducationBean();	
				    	         edb.setEducation_id(rs.getString("education_id"));
				    	         edb.setSchool_name(rs.getString("school_name"));
				    	         edb.setSkill(rs.getString("skill"));
				    	         edb.setLevel(rs.getString("level"));
				    	         edb.setStart(rs.getString("start_year"));
				    	         edb.setEnd(rs.getString("end_year"));
				    	         edb.setFacbook(rs.getString("facebook"));
				    	         edb.setEmail(rs.getString("email"));
				    	         ed.add(edb);
		               }
		}
	catch(Exception e)
	    {
	    	System.out.println("Error ==>> EducationService <<===>> ::DisplayEducation()".toString());
	    }
	finally
		{
		 MySQL.P_getClose();
        }
		
		
		return ed;
		
	}
	
	//=========   get Data on Update============
	public static EducationBean getDataTeacherEducation(String id){
		
		EducationBean ed=null;
		try
		{
			String sql = "SELECT * FROM t_teacher_edu WHERE education_id=? ";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
				{
				 ed =new EducationBean();
				 ed.setEducation_id(rs.getString("education_id"));
    	         ed.setSchool_name(rs.getString("school_name"));
    	         ed.setSkill(rs.getString("skill"));
    	         ed.setLevel(rs.getString("level"));
    	         ed.setStart(rs.getString("start_year"));
    	         ed.setEnd(rs.getString("end_year"));
    	         ed.setFacbook(rs.getString("facebook"));
    	         ed.setEmail(rs.getString("email"));
    	        
				}
			 
		}
		catch( Exception e)
		{
		
			System.out.println("Error ===>> EducationService ==>>::SelectdataTeacherEducation(EducationBean ed) ");
		}
		finally
		{
			MySQL.P_getClose();
		}
		
		return ed;
	}
	
//==========   insert data In table Teacher Education==========
	
	public static String InsertTeacherEducation(EducationBean ed){
		String mms="";
		try
		{
			
			String sql ="INSERT INTO t_teacher_edu VALUES(?,?,?,?,?,?,?,?)";
			PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
			ps.setString(1, ed.getEducation_id());
			ps.setString(2, ed.getSchool_name());
			ps.setString(3, ed.getSkill());
			ps.setString(4, ed.getLevel());
			ps.setString(5, ed.getStart());
			ps.setString(6, ed.getEnd());
			ps.setString(7, ed.getFacbook());
			ps.setString(8, ed.getEmail());
			 int  i = ps.executeUpdate();
			 if(i > 0)
			 {
				 mms="success";
			 }
		}catch( Exception e)
		{
			mms="error";
			System.out.println("Error ===>> EducationService ==>>::InsertTeacherEducation(EducationBean ed) ");
		}finally{
			MySQL.P_getClose();
		}
		
		return mms;
	}

//================== Get Max Id Education Teacher=====================
	
	public static String getNewEducationIdAuto()
    {
		        	       
		String code=null;
		  try
			  {
			             
			     String sql = "SELECT MAX(education_id) AS education_id FROM t_teacher_edu";
			     PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
			     ResultSet rs = ps.executeQuery();
			     if(rs.next())
				     {
				        String sCode = rs.getString("education_id");
				        String l_code = l_cutePrefix(sCode, "education");
				        int tmpCode = Integer.parseInt(l_code);
				        tmpCode ++;
				        code = "education";
				        code += String.format("%04d", tmpCode);
				     }         	
			   }
		  catch(Exception e)
			  {
			               	code = "education";
			               	code += String.format("%04d", 1);
			               	System.out.println("EducationTeacher::getNewEducationIdAuto() => " + e.toString());
			  }
		  finally
			  {
			               	MySQL.P_getClose();
			  }
		       		
		       		return code;
	}
		           
	private static String l_cutePrefix(String str, String pref)
	{
			if(str != null && pref != null && str.startsWith(pref))
				{
					return str.substring(pref.length());
				}
			return str;
	}
	//==============Delete From Education Teacher============
	public static String DeleteEducationTeacher( EducationBean ed)
	{
		String mms="";
		int i =0;
		try
		{
			String sql= "DELETE FROM t_teacher_edu WHERE education_id = ?";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			    ps.setString(1,ed.getEducation_id());
			    i = ps.executeUpdate();
			    if(i>0)
			    {
			    	mms="success";
			    }
			
		}
		catch(Exception e)
		{
			mms="error";
			System.out.println("error ===>> EducationService===>>::DeleteEducationTeacher( EducationBean id)".toString());
		}
		finally
		{
			MySQL.P_getClose();
		}
	  return mms;	
		
	}

	//================= Update Education =============
	public static String UpdateTeacherEducation(EducationBean ed){
		String mms="";
		try
		{
			int i=0;
			String sql ="UPDATE t_teacher_edu SET school_name=?,skill=?,level=?,start_year=?,end_year=?,facebook=?,email=? WHERE education_id=?";
			PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
			
			ps.setString(1, ed.getSchool_name());
			ps.setString(2, ed.getSkill());
			ps.setString(3, ed.getLevel());
			ps.setString(4, ed.getStart());
			ps.setString(5, ed.getEnd());
			ps.setString(6, ed.getFacbook());
			ps.setString(7, ed.getEmail());
			ps.setString(8, ed.getEducation_id());
			 i= ps.executeUpdate();
			 if(i>0)
			 {
				 mms="success";
				 System.out.println("Successs ===>> EducationService ==>>::UpdateTeacherEducation(EducationBean ed) ");
			 }
			 
		}
		catch( Exception e)
		{
			
			System.out.print(ed);
			mms="error";
			System.out.println("Error ===>> EducationService ==>>::UpdateTeacherEducation(EducationBean ed) ");
		}
		finally
		{
			MySQL.P_getClose();
		}
		
		return mms;
	}
}
