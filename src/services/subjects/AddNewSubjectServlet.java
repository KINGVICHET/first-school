package services.subjects;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SubjectBean;
import bean.SubjectCategoryBean;

@WebServlet("/AddNewSubjectServlet")
public class AddNewSubjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AddNewSubjectServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");//display HTML from servlet.
		
		String id = request.getParameter("subject_id");
		String sub = request.getParameter("subject_name");
		String catid = request.getParameter("category_id");
		
		SubjectBean sb = new SubjectBean();
		sb.setSubject_id(id);
		sb.setSubject_name(sub);
		
		SubjectCategoryBean subcb = new SubjectCategoryBean();
		subcb.setCategory_id(catid);
		
		sb.setScb(subcb);		
		
		String msg = db.services.SubjectService.addNewSubject(sb);
		HttpSession session = request.getSession(false);
		session.setAttribute("code", msg);
		response.sendRedirect("./view/detail/AddNewSubject?id="+sb.getSubject_id());
	}

}
