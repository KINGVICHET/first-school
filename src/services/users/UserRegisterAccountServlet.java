package services.users;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import bean.PermissionsBean;
import bean.UserBean;

@WebServlet("/UserRegisterAccountServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
						maxFileSize = 1024 * 1024 * 50,
						maxRequestSize = 1024 * 1024 * 100
)
public class UserRegisterAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public UserRegisterAccountServlet() {
        super();
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");//display HTML from servlet.	
		String userId = request.getParameter("user_id");
		String user = request.getParameter("username");
		String pass = request.getParameter("password");
		String hin = request.getParameter("hin");
		String quest = request.getParameter("question");
		String answer = request.getParameter("answer");
		String role_code =request.getParameter("role_code");
	    //int role_code =Integer.parseInt(request.getParameter("role_code"));
		// prepare save picture into project folder
		Part part = request.getPart("file");//c:\java\imgs\cham.jpg
		String fileName = fileNameFilter(part);
		System.out.println("fileName = " + fileName);
		String savePath = "\\images\\users\\";
		Path path = Paths.get("images/users");
		String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\users\\" + File.separator + fileName;
		System.out.println("url = " + urlPath);
		File fileSaveDirectory = new File(urlPath);
		part.write(urlPath + File.separator);
		UserBean ub = new UserBean();
		ub.setUser_id(userId);
		ub.setUser_name(user);
		ub.setPassword(pass);
		ub.setHin(hin);
		ub.setQuestion(quest);
		ub.setAnswer(answer);
		ub.setPhoto_name(fileName);
		ub.setPhoto_url(savePath);
		PermissionsBean pb = new PermissionsBean();
		pb.setRole_id(role_code);
		ub.setPb(pb);
		String url = db.services.UserService.P_addNewRegisterUserAccount(ub);
		response.sendRedirect(url);
	}
	public static String fileNameFilter(Part part){
		String contentDisplay = part.getHeader("content-disposition");
		System.out.println("contentDisplay = " + contentDisplay);
		String []items = contentDisplay.split(";");
		for(String str : items){
			if(str.trim().startsWith("filename")){
				String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
				System.out.println("fname = " + name);
				String []fname = name.split( Pattern.quote(File.separator) );
				return fname[fname.length - 1];
				//return str.substring(str.indexOf("=") + 2, str.length()-1);
			}
		}
		return "";
	}

}
