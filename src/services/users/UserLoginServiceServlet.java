package services.users;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.RoleBean;
import bean.UserBean;
import db.services.UserService;

@WebServlet("/UserLoginServiceServlet")
public class UserLoginServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UserLoginServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// In servet to get values from jps file or html file
		//we use get.Parameter("param_name");
		
		 String user = request.getParameter("username");
		    String pass = request.getParameter("password");
		    String photo_name = request.getParameter("photo_name");
		    String photo_url = request.getParameter("photo_url");
		    UserBean ub = new UserBean();
		    ub.setUser_name(user);
		    ub.setPassword(pass);
		    ub.setPhoto_name(photo_name);
		    ub.setPhoto_url(photo_url);
		    /*
		    String url = UserService.p_getLogin(ub);
		    System.out.println("url="+ url);
		    //Code redirect to jsp File
		    
		    if(!url.equalsIgnoreCase("./login_access")){
		    	//code redirect to any jsp file
		    	response.sendRedirect(url);
		    }else{
		    	request.setAttribute("msg","incorrect username and password");
		    	RequestDispatcher disp = request.getRequestDispatcher(url);
		    	disp.forward(request, response);
		    }  
		    */
		    
		    UserBean uBean = UserService.P_getLoginPermission(ub);
		    if(!uBean.getUrl().equalsIgnoreCase("./AccessSystem")){
		    	HttpSession session = request.getSession(true);
		    	session.setAttribute("user_name", uBean.getUser_name());
		    	session.setAttribute("photo_name", uBean.getPhoto_name());
		    	session.setAttribute("photo_url", uBean.getPhoto_url());
		    	
		    	RoleBean rb = uBean.getRb();
		    	session.setAttribute("role_code", rb.getRole_id());
		    	session.setAttribute("role_name", rb.getRole_name());
		    	
		    	response.sendRedirect(uBean.getUrl());
		    }else{
		    	request.setAttribute("msg","incorrect username and password");
		    	RequestDispatcher disp = request.getRequestDispatcher(uBean.getUrl());
		    	disp.forward(request, response);
		    }
		
	}

}
