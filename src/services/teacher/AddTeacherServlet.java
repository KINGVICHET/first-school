package services.teacher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import bean.AddressBean;
import bean.TeacherBean;
import db.services.TeacherService;

@WebServlet("/AddTeacherServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
						maxFileSize = 1024 * 1024 * 50,
						maxRequestSize = 1024 * 1024 * 100)
public class AddTeacherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddTeacherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");//display HTML from swiveled.
		
		String teacher_id = request.getParameter("teacher_id");
		String first_name = request.getParameter("first_name");
		String last_name = request.getParameter("last_name");
		String gender = request.getParameter("gender");
		String nationality =request.getParameter("nationality");
		String nationality_id =request.getParameter("nationality_id");
		String dob =request.getParameter("dob");
		String village_id= request.getParameter("village_id");
		String passport_no =request.getParameter("passport_no");
		String phone =request.getParameter("phone_number");
		String education_id =request.getParameter("education_id");
		String status =request.getParameter("status");
		
		Part part = request.getPart("file");
		String fileName = fileNameFilter(part);
		
		System.out.println("fileName = " + fileName);
		
		String savePath = "\\images\\teachers\\";
		
		Path path = Paths.get("images/teachers");
		
		String urlPath = "D:\\ThesisProject\\LMSystem\\WebContent\\images\\teachers\\" + File.separator + fileName;
		
		System.out.println("url = " + urlPath);
		
		File fileSaveDirectory = new File(urlPath);
		part.write(urlPath + File.separator);
		TeacherBean teb= new TeacherBean();
		
		
		teb.setT_id(teacher_id);
	   	teb.setT_fname(first_name);
	   	teb.setT_lname(last_name);
		teb.setT_gender(gender);
		
		AddressBean ab =new AddressBean();
	   	ab.setVillage_id(village_id);
	   	teb.setaddress_teacher(ab);
	   	
	   	teb.setT_dob(dob);
	   	teb.setT_phone(phone);
	   	teb.setNationality(nationality);
	    teb.setEducation_id(education_id);
	    teb.setPassport_no(passport_no);
	    teb.setNationality_id(nationality_id);
	    teb.setStustus(status);
	    teb.setT_photo(fileName);
	    teb.setPhoto_url(savePath);
	    
	    String msg = TeacherService.P_AddTeacher_InforMation(teb);
		HttpSession session = request.getSession(false);
		session.setAttribute("code", msg);
		response.sendRedirect("./view/Teacher/AddTeacher?id="+teb.getT_id());
	}
	
	public static String fileNameFilter(Part part){
		try{
			String contentDisplay = part.getHeader("content-disposition");
			
			System.out.println("contentDisplay = " + contentDisplay);
			String []items = contentDisplay.split(";");
			for(String str : items){
				if(str.trim().startsWith("filename")){
					String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
					System.out.println("fname = " + name);
					String []fname = name.split( Pattern.quote(File.separator) );
					return fname[fname.length - 1];
				}
			}	
		
		}catch(Exception e){
			e.getMessage();
		}
		return "";
	}	
}

