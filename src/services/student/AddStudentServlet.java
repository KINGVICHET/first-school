package services.student;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import bean.AddressBean;
import bean.StudentBean;

@WebServlet("/AddStudentServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
						maxFileSize = 1024 * 1024 * 50,
						maxRequestSize = 1024 * 1024 * 100
)
public class AddStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AddStudentServlet() {
        super();

    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");//display HTML from servlet.
		
		String id = request.getParameter("student_id");
		String fname = request.getParameter("first_name");
		String lname = request.getParameter("last_name");
		String gen = request.getParameter("gender");
		String nation = request.getParameter("nationality");
		
		String dob = request.getParameter("dob");
		String pob = request.getParameter("pob_village");
		String current = request.getParameter("current_village");
		String phone = request.getParameter("phone_number");
		String guardian = request.getParameter("guardian_name");
		
		// prepare save picture into project folder
		Part part = request.getPart("file");//c:\java\imgs\cham.jpg
		String fileName = fileNameFilter(part);
		System.out.println("fileName = " + fileName);
		String savePath = "\\images\\students\\";
		Path path = Paths.get("images/students");
		String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\students\\" + File.separator + fileName;
		System.out.println("url = " + urlPath);
		File fileSaveDirectory = new File(urlPath);
		part.write(urlPath + File.separator);
		
		StudentBean sb = new StudentBean();
		sb.setStudent_id(id);
		sb.setFirst_name(fname);
		sb.setLast_name(lname);
		sb.setGender(gen);
		sb.setNationality(nation);
		sb.setDob(dob);
		sb.setPob(pob);
		
		AddressBean ab = new AddressBean();
		ab.setVillage_id(current);
		sb.setAb(ab);
		
		sb.setPhone(phone);
		sb.setGuardian_id(guardian);
		sb.setPhoto_name(fileName);
		sb.setPhoto_url(savePath);
		
		String msg = db.services.StudentService.addNewStudent(sb);
		HttpSession session = request.getSession(false);
		session.setAttribute("code", msg);
		response.sendRedirect("./view/detail/AddAStudent?id="+sb.getStudent_id());
		
	}
	
	public static String fileNameFilter(Part part){
		String contentDisplay = part.getHeader("content-disposition");
		
		System.out.println("contentDisplay = " + contentDisplay);
		String []items = contentDisplay.split(";");
		for(String str : items){
			if(str.trim().startsWith("filename")){
				String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
				System.out.println("fname = " + name);
				String []fname = name.split( Pattern.quote(File.separator) );
				return fname[fname.length - 1];
				//return str.substring(str.indexOf("=") + 2, str.length()-1);
			}
		}
		return "";
	}

}
