package bean;

public class DistrictBean {
	private String district_id,district,province_id;
	private ProvinceBean pb;
	public ProvinceBean getPb() {
		return pb;
	}
	public void setPb(ProvinceBean pb) {
		this.pb = pb;
	}
	public DistrictBean(){}
	public DistrictBean(String id,String district,String proid){
		this.district_id = id;
		this.district = district;
		this.province_id = proid;
	}
	public String getDistrict_id() {
		return district_id;
	}
	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}
	public String getDistrict() {
		return district;
	}
	public String getProvince_id() {
		return province_id;
	}
	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
}
