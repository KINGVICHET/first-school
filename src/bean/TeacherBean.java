package bean;

 
public class TeacherBean  {
	
	private String t_id, t_fname,t_lname,t_gender,pob,current_address,t_dob,t_phone,t_photo,nationality,Education_id ,passport_no,stustus ,nationality_id;
    
	private TeacherBean tb;
	private AddressBean address_teacher;
	
	private SubjectTeacherBean stb;
	private SubjectCategoryBean scb;
	
	    public SubjectTeacherBean getStb() {
		return stb;
	}

	public void setStb(SubjectTeacherBean stb) {
		this.stb = stb;
	}

	public SubjectCategoryBean getScb() {
		return scb;
	}

	public void setScb(SubjectCategoryBean scb) {
		this.scb = scb;
	}

		public AddressBean getaddress_teacher() {
		return address_teacher;
	}

	public String getNationality_id() {
			return nationality_id;
		}

		public void setNationality_id(String nationality_id) {
			this.nationality_id = nationality_id;
		}

	public void setaddress_teacher(AddressBean address_teacher) {
		this.address_teacher=address_teacher;
	}

		
	
	private  String photo_url ,url;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public EducationBean getEb() {
		return Eb;
	}

	public void setEb(EducationBean eb) {
		Eb = eb;
	}

	private EducationBean Eb;
	
	public TeacherBean getTb() {
		return tb;
	}

	public void setTb(TeacherBean tb) {
		this.tb = tb;
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public String getT_fname() {
		return t_fname;
	}

	public void setT_fname(String t_fname) {
		this.t_fname = t_fname;
	}

	public String getT_lname() {
		return t_lname;
	}

	public void setT_lname(String t_lname) {
		this.t_lname = t_lname;
	}

	public String getT_gender() {
		return t_gender;
	}

	public void setT_gender(String t_gender) {
		this.t_gender = t_gender;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public String getT_dob() {
		return t_dob;
	}

	public void setT_dob(String t_dob) {
		this.t_dob = t_dob;
	}

	public String getT_phone() {
		return t_phone;
	}

	public void setT_phone(String t_phone) {
		this.t_phone = t_phone;
	}

	public String getT_photo() {
		return t_photo;
	}

	public void setT_photo(String t_photo) {
		this.t_photo = t_photo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getEducation_id() {
		return Education_id;
	}

	public void setEducation_id(String education_id) {
		Education_id = education_id;
	}

	public String getPassport_no() {
		return passport_no;
	}

	public void setPassport_no(String passport_no) {
		this.passport_no = passport_no;
	}

	public String getStustus() {
		return stustus;
	}

	public void setStustus(String stustus) {
		this.stustus = stustus;
	}

	@Override
	public String toString() {
		return "teacherBeen [t_id=" + t_id + ", t_fname=" + t_fname + ", t_lname=" + t_lname + ", t_gender=" + t_gender
				+ ", t_add_id=" + pob + ", t_dob=" + t_dob + ", t_phone=" + t_phone + ", t_photo=" + t_photo
				+ ", nationality=" + nationality + ", Education_id=" + Education_id + ", passport_no=" + passport_no
				+ ", stustus=" + stustus + "]";
	}
	
	public TeacherBean() {
		
	}
	
public TeacherBean(String t_id,String t_fname ,String t_lname ,String t_gender,String pob,String current_address ,String t_dob,String t_phone,String t_photo,String nationality ,String Education_id ,String passport_no, String stustus ,String url) {
	
		this.t_gender = t_gender;
		this.t_lname = t_lname;
		this.t_fname =t_fname;
		this.t_lname=t_lname;
		this.pob=pob;
		this.current_address = current_address;
		this.t_dob =t_dob;
		this.t_phone=t_phone;
		this.t_photo=t_photo;
		this.nationality=nationality;
		this.Education_id =Education_id;
		this.passport_no=passport_no;
		this.stustus=stustus;
		this.url=url;
		
	}

public String getCurrent_address() {
	return current_address;
}

public void setCurrent_address(String current_address) {
	this.current_address = current_address;
}

public AddressBean getAddress_teacher() {
	return address_teacher;
}

public void setAddress_teacher(AddressBean address_teacher) {
	this.address_teacher = address_teacher;
}



	
}
