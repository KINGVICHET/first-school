package bean;

public class EducationBean {

	private String education_id ,school_name,skill,level,start,end,facbook,email;

	public String getFacbook() {
		return facbook;
	}

	public void setFacbook(String facbook) {
		this.facbook = facbook;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEducation_id() {
		return education_id;
	}

	public void setEducation_id(String education_id) {
		this.education_id = education_id;
	}

	public String getSchool_name() {
		return school_name;
	}

	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public EducationBean() {
		
	}
	


public EducationBean(String education_id, String school_name, String skill, String level, String start, String end,String facbook, String email) {
		super();
		this.education_id = education_id;
		this.school_name = school_name;
		this.skill = skill;
		this.level = level;
		this.start = start;
		this.end = end;
		this.facbook = facbook;
		this.email = email;
	}

@Override
public String toString() {
	return "EducationBean [education_id=" + education_id + ", school_name=" + school_name + ", skill=" + skill
			+ ", level=" + level + ", start=" + start + ", end=" + end + ", facbook=" + facbook + ", email=" + email
			+ ", getFacbook()=" + getFacbook() + ", getEmail()=" + getEmail() + ", getEducation_id()="
			+ getEducation_id() + ", getSchool_name()=" + getSchool_name() + ", getSkill()=" + getSkill()
			+ ", getLevel()=" + getLevel() + ", getStart()=" + getStart() + ", getEnd()=" + getEnd() + "]";
}



	
	
	
	
	
	
	
	
	
	
}
